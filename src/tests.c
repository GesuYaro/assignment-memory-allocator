#define VERY_BIG_QUERY 16367
#define MAP_ANONYMOUS 32

#include "tests.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "mem.h"

static void print_test_header(const int32_t test_number) {
    fprintf(stderr, "----------------- test № %" PRId32 " -----------------\n", test_number);
}

void test1() {
    print_test_header(1);
    int64_t* array = _malloc(sizeof(int64_t) * 5);
    fprintf(stderr, "allocate from here: %" PRIXPTR "\n", (uintptr_t) array);
    debug_heap(stderr, HEAP_START);
    for (size_t i = 0; i < 5; i++) {
        array[i] = i * 50 + i;
    }
    fprintf(stderr, "array containing:\n");
    for (size_t i = 0; i < 5; i++) {
        fprintf(stderr, "%" PRId64 "\n", array[i]);
    }
    _free(array);
    fprintf(stderr, "memory freed\n");
    debug_heap(stderr, HEAP_START);
}

void test2() {
    print_test_header(2);
    int32_t** marray = _malloc(sizeof(int64_t *) * 6);
    debug_heap(stderr, HEAP_START);

    for (size_t i = 0; i < 6; i++) {
        marray[i] = _malloc(sizeof(int64_t) * 100);
        for (size_t j = 0; j < 100; j++) {
            marray[i][j] = i * 100 + j;
        }
    }
    fprintf(stderr, "create array 6*100\n");
    debug_heap(stderr, HEAP_START);

    fprintf(stderr, "execute test 1\n");
    test1();

    fprintf(stderr, "free line № 2 of 2d array\n");
    _free(marray[2]);
    debug_heap(stderr, HEAP_START);

    fprintf(stderr, "free line № 1 of 2d array\n");
    _free(marray[1]);
    debug_heap(stderr, HEAP_START);

    fprintf(stderr, "execute test 1\n");
    test1();

    for (size_t i = 0; i < 6; i++) {
        _free(marray[i]);
    }
    fprintf(stderr, "lines cleared\n");
    debug_heap(stderr, HEAP_START);
    _free(marray);
    fprintf(stderr, "memory freed\n");
    debug_heap(stderr, HEAP_START);
}

void test3() {
    print_test_header(3);

    fprintf(stderr, "allocate 8000\n");
    void* b1 = _malloc(8000);
    debug_heap(stderr, HEAP_START);

    fprintf(stderr, "allocate 1000\n");
    void* b2 = _malloc(1000);
    debug_heap(stderr, HEAP_START);

    _free(b2);
    _free(b1);
    fprintf(stderr, "memory freed\n");
    debug_heap(stderr, HEAP_START);

    fprintf(stderr, "allocate 10000\n");
    void* b3 = _malloc(10000);
    debug_heap(stderr, HEAP_START);

    _free(b3);
    fprintf(stderr, "memory freed\n");
    debug_heap(stderr, HEAP_START);
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

void test4() {
    print_test_header(4);

    fprintf(stderr, "allocate very big query\n");
    void* const b1 = _malloc(VERY_BIG_QUERY);
    debug_heap(stderr, HEAP_START);

    fprintf(stderr, "occupy memory after our region\n");
    void* const garbage = (char*) b1 + VERY_BIG_QUERY;
    map_pages(garbage, 4096, 0);

    fprintf(stderr, "allocate memory after occupied memory\n");
    void* const b2 = _malloc(8175);
    debug_heap(stderr, HEAP_START);

    _free(b2);
    _free(b1);
    fprintf(stderr, "memory freed\n");
    debug_heap(stderr, HEAP_START);
}

